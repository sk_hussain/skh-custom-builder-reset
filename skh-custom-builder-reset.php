<?php
/**
 *
 * @wordpress-plugin
 * Plugin Name:       Custom Builder Reset Update Test
 * Plugin URI:        www.shekhkamal.com
 * Description:       Reset Builder CSS. It will help not to override custom theme css.
 * Version:           1.0.0
 * Author:            Shekh Kamal Hussain
 * Author URI:        www.shekhkamal.com
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       skh-custom-builder-reset
 * Domain Path:       /languages
 */


/* DELETE THIS AFTER REPLACING THE TEXT
**
// 1:Replace skh-custom-builder-reset with "plugin-name"
** This is for Css and js namespace
**
**
// 2:Replace skh_custom_builder_reset with "plugin_name"
** This is for Function Call
**
** 3:Replace CUSTOM_BUILDER_RESET_PLUGIN_DRI with "PLUGIN_NAME_DIR"
**
*/


/**
* SET UP =================================
*/


// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

define( 'CUSTOM_BUILDER_RESET_PLUGIN_DRI', plugin_dir_url(__FILE__) );


/**
* INCLUDES =================================
*/
require_once plugin_dir_path( __FILE__ ) . 'includes/load-scripts.php';



/**
* HOOKS =================================
*/

/**
 * The code that runs during plugin activation.
 */
function skh_custom_builder_reset_activate(){
	require_once plugin_dir_path( __FILE__ ) . 'includes/activator.php';
}

/**
 * The code that runs during plugin deactivation.
 */
function skh_custom_builder_reset_deactivate(){
	require_once plugin_dir_path( __FILE__ ) . 'includes/deactivator.php';
}

/**
 * The code that runs when plugin initialize
 */
function skh_custom_builder_reset_init(){
	require_once plugin_dir_path( __FILE__ ) . 'includes/init.php';
}


register_activation_hook( __FILE__, 'skh_custom_builder_reset_activate');

add_action('init','skh_custom_builder_reset_init');

register_deactivation_hook( __FILE__, 'skh_custom_builder_reset_deactivate');



/**
* SHORTCODES =================================
*/