<?php




/**
* Add Admin Scripts
* Register all of the hooks related to the admin area functionality
* of the plugin.
*
*/
function skh_custom_builder_reset_admin_scripts() {

    //Css
    wp_enqueue_style('skh_custom_builder_reset_admin-css', CUSTOM_BUILDER_RESET_PLUGIN_DRI . 'admin/css/skh-custom-builder-reset-admin.css');

    //Js
    wp_enqueue_script('skh_custom_builder_reset_admin-js', CUSTOM_BUILDER_RESET_PLUGIN_DRI . 'admin/js/skh-custom-builder-reset-admin.js', array('jquery'), '1.0', true);
}
//add_action('admin_enqueue_scripts', 'skh_custom_builder_reset_admin_scripts');




/**
* Add Public Scripts
* Register all of the hooks related to the public area functionality
* of the plugin.
*
*/
function skh_custom_builder_reset_public_scripts() {

    wp_dequeue_style('et-builder-modules-style');
    //Css
    wp_enqueue_style('skh_custom_builder_reset_public-css', CUSTOM_BUILDER_RESET_PLUGIN_DRI . 'public/css/skh-custom-builder-reset-public.css');

    //Js
   // wp_enqueue_script('skh_custom_builder_reset_public-js', CUSTOM_BUILDER_RESET_PLUGIN_DRI . 'public/js/skh-custom-builder-reset-public.js', array('jquery'), '1.0', true);
}
add_action('wp_enqueue_scripts', 'skh_custom_builder_reset_public_scripts',99);